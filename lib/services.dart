import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pointycastle/digests/sha256.dart';
import 'package:pointycastle/macs/hmac.dart';
import 'package:pointycastle/pointycastle.dart';
import 'package:pointycastle/signers/ecdsa_signer.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:walletconnect_flutter_v2/walletconnect_flutter_v2.dart';
import 'package:web3dart/crypto.dart';
import 'package:web3dart/web3dart.dart';

String url = "http://35.174.138.211:9080";

ConnectResponse? resp;
SessionData? session;
Web3App? wcClient;

creatConnetion() async {
  if (wcClient != null) {
    return;
  }
  wcClient = await Web3App.createInstance(
    projectId: '1958d35988b449e517442899fd5b9e9a',
    metadata: const PairingMetadata(
      name: 'dApp (Requester)',
      description: 'A dapp that can request that transactions be signed',
      url: 'https://walletconnect.com',
      icons: ['https://avatars.githubusercontent.com/u/37784886'],
    ),
  );
}

Future<SessionData?> connectWallet() async {
  try {
    await creatConnetion();

    resp = await wcClient!.connect(requiredNamespaces: {
      'eip155': const RequiredNamespace(chains: [
        'eip155:1'
      ], methods: [
        'personal_sign',
        'eth_requestAccounts',
        'eth_signTransaction',
      ], events: [
        "connect",
        'personal_sign',
      ])
    });
    Uri? uri = resp!.uri;

    await _launchUrl(uri!);
// Once you've display the URI, you can wait for the future, and hide the QR code once you've received session data
    session = await resp!.session.future;

    print(
        'SESSIONDATA ========================================== ${session!.namespaces['eip155']!.accounts[0]}');

    return session;
  } catch (e) {
    print(e);
    return null;
  }
}

Future requestAccount(
    SessionData session, Web3App wcClient, dynamic resp, String address, String nonce) async {
  Uri? uri = resp.uri;
  String c = utf8ToHexString(nonce);

  await _launchUrl(uri!);

  final dynamic accountResponse = await wcClient.request(
    topic: session.topic,
    chainId: 'eip155:1',
    request: SessionRequestParams(
      method: 'eth_requestAccounts',
      params: [c, address],
    ),
  );
  // this have the accounts addresses
  final List<dynamic> accounts = accountResponse;

  return accounts;
}

Future signConnect(SessionData session, Web3App wcClient, dynamic resp, String accountAddress,
    String nonce) async {
  Uri? uri = resp.uri;
  String c = utf8ToHexString(nonce);

  await _launchUrl(uri!);

  final dynamic signResponse = await wcClient.request(
    topic: session.topic,
    chainId: 'eip155:1',
    request: SessionRequestParams(
      method: 'personal_sign',
      params: [c, accountAddress],
    ),
  );
  final dynamic sign = signResponse;
  print('SignResponse ======================================== ${sign}');
  return sign;
  // authanticate(session, wcClient, resp);
}

register(String publicKey, String nonce, String signature) async {
  var address;
  var response;
  var res;
  var responce;
  var resNonce;
  var s;
  // var privateKey;
  var sHex;

  try {
    response = await http.post(Uri.parse('$url/signup'),
        body: jsonEncode({
          "address": publicKey,
          "username": 'user${Random().nextInt(999999)}',
          "nonce": int.tryParse(nonce),
        }),
        headers: {
          'Content-type': 'application/json',
        });

    res = jsonDecode(response.body);
    print(res.toString());

    if (response.statusCode == 200) {
      responce = await http.get(Uri.parse('$url/$publicKey/nonce'));
      resNonce = jsonDecode(responce.body);

      print(resNonce.toString());

      if (responce.statusCode == 200) {
        var check = await http.get(Uri.parse('$url/$publicKey'));

        var resChek = jsonDecode(check.body);
        print(resChek);

        if (check.statusCode == 200) {
          var respSignature = await http.post(Uri.parse('$url/signature/$publicKey'),
              body: jsonEncode({"signature": signature}),
              headers: {
                'Content-type': 'application/json',
              });

          var resS = jsonDecode(respSignature.body);
          print(resS);
        }
      }
    }
  } catch (e) {
    print(e);
  }
}

Future<void> _launchUrl(Uri url) async {
  if (!await launchUrl(url)) {}
}

String utf8ToHexString(String inputString) {
  List<int> utf8Bytes = inputString.codeUnits;
  String hexString = '0x' + utf8Bytes.map((byte) => byte.toRadixString(16).padLeft(2, '0')).join();
  return hexString;
}
