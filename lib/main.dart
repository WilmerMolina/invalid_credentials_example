import 'dart:math';

import 'package:flutter/material.dart';
import 'package:invalid_credential_error/services.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // TRY THIS: Try running your application with "flutter run". You'll see
        // the application has a blue toolbar. Then, without quitting the app,
        // try changing the seedColor in the colorScheme below to Colors.green
        // and then invoke "hot reload" (save your changes or press the "hot
        // reload" button in a Flutter-supported IDE, or press "r" if you used
        // the command line to start the app).
        //
        // Notice that the counter didn't reset back to zero; the application
        // state is not lost during the reload. To reset the state, use hot
        // restart instead.
        //
        // This works for code too, not just values: Most code changes can be
        // tested with just a hot reload.
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  String nonce = Random().nextInt(999999).toString();
  String address = '';
  String signString = '';
  void connectWithMetamask() async {
    await connectWallet().then((value) {
      List<String> addressSplit = value!.namespaces['eip155']!.accounts[0].split(':');
      setState(() {
        address = addressSplit[2];
      });

      requestAccount(value, wcClient!, resp, address, nonce).then((value) {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(onPressed: connectWithMetamask, child: Text('connect')),
            if (address != '')
              Text(
                address,
              ),
            if (address != '')
              ElevatedButton(
                  onPressed: () {
                    signConnect(session!, wcClient!, resp, address, nonce).then((value) {
                      setState(() {
                        signString = value;
                      });
                    });
                  },
                  child: Text('sign')),
            if (signString != '') Text(signString),
            if (signString != '')
              ElevatedButton(
                  onPressed: () {
                    register(address, nonce, signString);
                  },
                  child: Text('register')),
          ],
        ),
      ),
    );
  }
}
